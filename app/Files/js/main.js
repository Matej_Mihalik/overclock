var globalMoment = null;
var globalDataStorage = null;
var globalClockRowTemplate = null;
var globalTimezones = {};
var globalAppPadding = 50;
var globalAllowAppAnimation = false;
var globalAppVisible = true;
var globalHasFocus = false;
var globalGazePoint = null;
var globalLocalTimezoneId = 'Local Time';

$(document).ready(function() {
	init();
	initEyeX();
	initAlarmWindow();
});

function init() {
	setGlobals();
	bindUI();
	applyStoredData();

	setTimeout(function() {
		globalAllowAppAnimation = true;
		if(!globalHasFocus) {
			hideApp();
		}
	}, 5000);
}

function setGlobals() {
	globalMoment = moment();

	globalDataStorage = new DataStorage();

	globalClockRowTemplate = $('.clock-row').remove();

	globalTimezones[humanize(globalLocalTimezoneId)] = globalLocalTimezoneId;
	moment.tz.names().forEach(function(timezone) {
		globalTimezones[humanize(timezone)] = timezone;
	});
}

function bindUI() {
	bindMouseHoverFallback();

	$('.switcher-button').bind('click', function() {
		switchFormat(this);
	});

	$('.minimize').bind('click', function() {
		windowMinimize();
	});

	$('.close').bind('click', function() {
		windowClose();
	});

	$('.add-row').bind('click', function() {
		addClock().children('.timezone').focus();
		saveClockData();
	});
}

function applyStoredData() {
	$('.switcher-button[data-value="'+globalDataStorage.getMilitaryFormat()+'"]').addClass('switcher-button-active');

	globalDataStorage.getClocks().forEach(function(clock) {
		addClock(clock.timezone, clock.alarm);
	});
}

function addClock(timezone, alarm) {
	timezone = timezone ? timezone : '';
	alarm = alarm ? new Date(alarm) : null;
	var clockRow = globalClockRowTemplate.clone();
	var offsetMoment = globalMoment.clone();
	setMomentTimezone(offsetMoment, timezone);
	var previousOffset = globalMoment.zone();
	var nextOffset = offsetMoment.zone();

	var clock = clockRow.children('.clock').FlipClock({
		autoStart: false,
		clockFace: globalDataStorage.getMilitaryFormat() ? 'TwentyFourHourClock' : 'TwelveHourClock'
	});
	clockRow.children('.clock').data('clock', clock);

	initDateTimePicker(clockRow.children('.alarm'), globalDataStorage.getMilitaryFormat());
	clockRow.children('.alarm').datetimepicker('setDate', alarm);
	clockRow.children('.ui-datepicker-trigger').toggleClass('alarm-set',alarm!==null);

	var autocomplete = clockRow.children('.timezone').autocomplete({
		source: Object.keys(globalTimezones),
		minLength: 0,
		change: function(event, ui) {
			var element = $(this);

			offsetMoment = moment().subtract(1, 's');
			var diff = globalMoment.milliseconds() - offsetMoment.milliseconds();
			if(globalMoment.milliseconds()<offsetMoment.milliseconds()) {
				diff += 1000;
				offsetMoment.add(1, 's');
			}

			setTimeout(function() {
				var valid = globalTimezones.hasOwnProperty(element.val());

				element.toggleClass('input-error', !valid);
				clock.$el.toggleClass('clock-error', !valid);

				if(valid) {
					previousOffset = offsetMoment.zone();
					setMomentTimezone(offsetMoment, element.val());
					nextOffset = offsetMoment.zone();

					offsetMoment.add(previousOffset - nextOffset, 'm');
					clock.setTime(offsetMoment.toDate());

					if(!clock.running) {
						clock.start();
					}
				}
				else if(clock.running) {
					clock.stop();
				}
			}, diff);

			if(ui.item!==undefined) {
				saveClockData();
				saveAlarmData();
			}
		},
		select: function(event, ui) {
			$(this).val(ui.item.value);
			$(this).trigger('blur');
		}
	});
	autocomplete.val(timezone).trigger('blur');

	clockRow.children('.remove-row').bind('click', function() {
		removeClock(this);
	});

	clockRow.insertBefore('.add-row');

	windowResize();

	return clockRow;
}

function removeClock(target) {
	$(target).parent().remove();

	windowResize();

	saveClockData();
}

function switchFormat(target) {
	var element = $(target);

	if(element.hasClass('switcher-button-active')) {
		return true;
	}

	element.siblings().removeClass('switcher-button-active');
	element.addClass('switcher-button-active');

	var militaryFormat = element.data('value');

	initDateTimePicker($('.alarm').datetimepicker('destroy'), militaryFormat);
	$('.alarm').each(function() {
		var element = $(this);

		element.siblings('.ui-datepicker-trigger').toggleClass('alarm-set', element.datetimepicker('getDate')!==null);
	});

	$('.clock').each(function() {
		var clock = $(this).data('clock');
		var clockRunning = clock.running;

		clock.loadClockFace(militaryFormat ? 'TwentyFourHourClock' : 'TwelveHourClock');
		if(!clockRunning) {
			clock.stop();
		}
	});

	globalDataStorage.setMilitaryFormat(militaryFormat);
}

function initDateTimePicker(elements, militaryFormat) {
	elements.datetimepicker({
		buttonImage: 'img/blank.png',
		buttonImageOnly: true,
		buttonText: 'Alarm',
		closeText: 'Set',
		currentText: 'Clear',
		pickerTimeFormat: militaryFormat ? 'HH:mm' : 'hh:mm TT',
		showButtonPanel: true,
		showOn: 'button',
		onChangeMonthYear: function(year, month, inst) {
			setTimeout(function() {
				inst.dpDiv.find('.ui-datepicker-current').unbind('click').bind('click', function() {
					initDateTimePicker(inst.input.datetimepicker('hide').datetimepicker('setDate', null).datetimepicker('destroy'), globalDataStorage.getMilitaryFormat());
				});
			}, 1);
		},
		beforeShow: function(input, inst) {
			setTimeout(function() {
				inst.dpDiv.find('.ui-datepicker-current').unbind('click').bind('click', function() {
					initDateTimePicker(inst.input.datetimepicker('hide').datetimepicker('setDate', null).datetimepicker('destroy'), globalDataStorage.getMilitaryFormat());
				});
			}, 1);
		},
		onClose: function() {
			var element = $(this);

			element.siblings('.ui-datepicker-trigger').toggleClass('alarm-set', element.datetimepicker('getDate')!==null);

			saveClockData();
			saveAlarmData();
		}
	});
}

function saveClockData() {
	globalDataStorage.setClocks($('.clock-row').map(function() {
		var timezone = $(this).children('.timezone').val();
		var alarm = $(this).children('.alarm').datetimepicker('getDate');

		return {
			alarm: alarm ? alarm.getTime() : null,
			clockface: 'flipclock',
			timezone: timezone
		};
	}).get());
}

function saveAlarmData() {
	globalDataStorage.setAlarms($('.clock-row').map(function() {
		var alarm = $(this).children('.alarm').datetimepicker('getDate');
		var timezone = $(this).children('.timezone').val();

		if(alarm===null || !globalTimezones.hasOwnProperty(timezone)) {
			return null;
		}

		return {
			alarm: alarm,
			humanTimezone: timezone,
			realTimezone: globalTimezones[timezone]
		};
	}).get());
}

function showApp() {
	globalHasFocus = true;
	if(!globalAppVisible && globalAllowAppAnimation) {
		animateIn();
	}
}

function hideApp() {
	globalHasFocus = false;
	if(globalAppVisible && globalAllowAppAnimation) {
		animateOut();
	}
}

function animateIn() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	var containerWidth = 340;
	$('.container').css('overflow-y', 'auto').width(containerWidth);
	var scrollbarWidth = containerWidth - $('.header').outerWidth();
	$('.container').width(containerWidth + scrollbarWidth);
	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			var windowHeight = $('html').height();
			var windowWidth = containerWidth + globalAppPadding;
			overwolf.windows.changeSize(result.window.id, windowWidth + scrollbarWidth, windowHeight, function() {
				globalAppVisible = true;
			});
		}
	});
}

function animateOut() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	$('.container').css('overflow-y', 'hidden').width(1);
	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			var windowHeight = $('html').height();
			var windowWidth = 1 + globalAppPadding;
			overwolf.windows.changeSize(result.window.id, windowWidth, windowHeight, function() {
				globalAppVisible = false;
			});
		}
	});
}

function setMomentTimezone(moment, timezone) {
	if(timezone===globalLocalTimezoneId) {
		moment.local();
	}
	else {
		moment.tz(globalTimezones[timezone]);
	}
}

function humanize(string) {
	return string.replace(/_/g, ' ').replace(/[a-z]([A-Z])/g, ' $1');
}

function bindMouseHoverFallback() {
	$('.container').bind('mouseenter', function(e) {
		if(e.pageX>0 || e.pageY>0) {
			showApp();
		}
	}).bind('mouseleave', function(e) {
		if((e.pageX>0 || e.pageY>0) && (e.relatedTarget===null || (!$(e.relatedTarget).hasClass('ui-menu') && !$(e.relatedTarget).hasClass('ui-menu-item') && !$(e.relatedTarget).hasClass('ui-datepicker') && !$.contains(document.getElementById('ui-datepicker-div'), e.relatedTarget)))) {
			hideApp();
		}
	});
}

function unbindMouseHoverFallback() {
	$('.container').unbind('mouseenter').unbind('mouseleave');
}

function windowResize() {
	var containerWidth = 340;
	var windowHeight = $('html').height();
	var windowWidth = containerWidth + globalAppPadding;
	var scrollbarWidth = $('.container').width() - $('.header').outerWidth();
	$('.container').width(containerWidth + scrollbarWidth);

	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			overwolf.windows.changeSize(result.window.id, windowWidth + scrollbarWidth, windowHeight);
		}
	});
}

function windowMinimize() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			overwolf.windows.minimize(result.window.id);
		}
	});
}

function windowClose() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			overwolf.windows.close(result.window.id);
		}
	});
}

function initEyeX() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.extensions.current.getExtraObject('eyex', function(result) {
		if(result.status==='success') {
			var eyex = result.object;
			eyex.onUserPresenceChanged.addListener(onUserPresenceChanged);
			eyex.onGazePoint.addListener(onGazePointChanged);
			eyex.init();
		}
	});
}

function onUserPresenceChanged(state) {
	if(state.IsValid && state.Value==='Present') {
		unbindMouseHoverFallback();
	}
	else {
		bindMouseHoverFallback();
	}
}

function onGazePointChanged(gazePoint) {
	var smoothGazePoint = filterGazePoint(gazePoint);
	if(smoothGazePoint.X<=$(window).width() && smoothGazePoint.Y<=$(window).height()) {
		showApp();
	}
	else {
		hideApp();
	}
}

function filterGazePoint(gazePoint) {
	// Throw away historic gaze point if its timestamp is too old
	var oldPointThresholdMilliseconds = 200;

	if(globalGazePoint===null || (gazePoint.Timestamp-globalGazePoint.Timestamp)>oldPointThresholdMilliseconds) {
		globalGazePoint = {X:gazePoint.X|0, Y:gazePoint.Y|0, Timestamp:gazePoint.Timestamp};
	}

	// Exponential smooting alpha value between 0 and 1:
	// - Low alpha value gives slow and stable movement
	// - High alpha value gives fast but nervous movement
	// For example: If alpha is 0.2, 20% of the new point is used in the weighted average
	var alpha = 0.3;

	// Distance threshold in pixels:
	// If the gaze point moves shorter than this distance, it can be due
	// to involuntary eye movements such as micro-saccades or just noise
	// in the system. In this case, the alpha value should be decreased.
	// Ideally, this threshold should be based on the eye-gaze vector angle
	// but in this implementation we chose pixels for simplicity reasons.
	var distanceTreshold = 60;

	distanceBetweenOldAndNewPoint = pointDistance(globalGazePoint, gazePoint);
	if(distanceBetweenOldAndNewPoint<distanceTreshold) {
		alpha = alpha/(distanceTreshold - distanceBetweenOldAndNewPoint);
	}

	// Exponential smoothing algorithm: resultPoint = newPoint * alpha + oldPoint * (1 - alpha)
	newGazePoint = {
		X:(gazePoint.X * alpha + globalGazePoint.X * (1 - alpha)),
		Y: (gazePoint.Y * alpha + globalGazePoint.Y * (1 - alpha)),
		Timestamp: gazePoint.Timestamp
	};

	// Save the filtered gaze point in memory to use it next time a new gaze point arrives.
	globalGazePoint = newGazePoint;

	return newGazePoint;
}

function pointDistance(point1, point2) {
	var xs = 0;
	var ys = 0;

	xs = point2.X - point1.X;
	xs = xs * xs;

	ys = point2.Y - point1.Y;
	ys = ys * ys;

	return Math.sqrt(xs + ys);
}

function initAlarmWindow() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.windows.obtainDeclaredWindow('AlarmWindow', function(result) {
		if(result.status==='success') {
			overwolf.windows.restore(result.window.id, function(result) {
				if(result.status==='success') {
					overwolf.windows.minimize(result.window_id);
				}
			});
		}
	});
}
