function DataStorage() {
	this.militaryFormat = true;
	this.clocks = [
		{
			timezone: globalLocalTimezoneId,
			alarm: null
		}
	];
	this.alarms = [];

	this.readData();
}

DataStorage.prototype.readData = function() {
	for(var property in this) {
		var prefixedProperty = 'occ-'+property;
		if(this.hasOwnProperty(property) && localStorage.getItem(prefixedProperty)!==null) {
			this[property] = JSON.parse(localStorage.getItem(prefixedProperty));
		}
	}
};

DataStorage.prototype.writeData = function() {
	for(var property in this) {
		var prefixedProperty = 'occ-'+property;
		if(this.hasOwnProperty(property)) {
			localStorage.setItem(prefixedProperty, JSON.stringify(this[property]));
		}
	}
};

DataStorage.prototype.getMilitaryFormat = function() {
	return this.militaryFormat;
};

DataStorage.prototype.setMilitaryFormat = function(value) {
	this.militaryFormat = value;
	this.writeData();
};

DataStorage.prototype.getClocks = function() {
	return this.clocks;
};

DataStorage.prototype.setClocks = function(value) {
	this.clocks = value;
	this.writeData();
};

DataStorage.prototype.getAlarms = function() {
	return this.alarms;
};

DataStorage.prototype.setAlarms = function(value) {
	this.alarms = value;
	this.writeData();
};
