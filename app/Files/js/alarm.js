var globalDataStorage = null;
var globalAlarmRowTemplate = null;
var globalAudioFile = null;
var globalAlarmIds = [];
var globalHasFocus = false;
var globalGazePoint = null;
var globalLocalTimezoneId = 'Local Time';

$(document).ready(function() {
	init();
	initEyeX();
});

function init() {
	setGlobals();
	applyStoredData();
	bindStorageListeners();
}

function setGlobals() {
	globalDataStorage = new DataStorage();

	globalAlarmRowTemplate = $('.alarm-row').remove();

	globalAudioFile = new Audio('audio/alarm.wav');
	globalAudioFile.loop = true;
}

function applyStoredData() {
	setAlarms(globalDataStorage.getAlarms());
}

function bindStorageListeners() {
	window.addEventListener('storage', function(event) {
		switch(event.key) {
			case 'occ-alarms':
				clearAlarms();
				setAlarms(JSON.parse(event.newValue));
				break;
			case 'occ-militaryFormat':
				var oldFormat = JSON.parse(event.oldValue) ? 'HH:mm' : 'hh:mm A';
				var newFormat = JSON.parse(event.newValue) ? 'HH:mm' : 'hh:mm A';

				$('.alarm-row').children('.time').each(function() {
					var element = $(this);

					element.text(moment(element.text(), oldFormat).format(newFormat));
				});
				break;
			default:
				break;
		}
	});
}

function clearAlarms() {
	globalAlarmIds.forEach(function(id) {
		clearTimeout(id);
	});
	globalAlarmIds = [];
}

function setAlarms(alarms) {
	alarms.forEach(function(alarm) {
		setAlarm(alarm);
	});
}

function setAlarm(alarm, diffOverride) {
	var diff = diffOverride;

	if(typeof diff==='undefined') {
		var alarmMoment = moment(alarm.alarm);
		var previousOffset = alarmMoment.zone();
		setMomentTimezone(alarmMoment, alarm.realTimezone);
		var nextOffset = alarmMoment.zone();
		alarmMoment.add(nextOffset - previousOffset, 'm');
		diff = alarmMoment.toDate() - new Date();
	}

	if(diff>=0) {
		var alarmRow = globalAlarmRowTemplate.clone();
		var alarmId = setTimeout(function() {
			alarmRow.children('.time').text(moment(alarm.alarm).format(globalDataStorage.getMilitaryFormat() ? 'HH:mm' : 'hh:mm A'));

			alarmRow.children('.timezone').text(alarm.humanTimezone);

			alarmRow.children('.snooze').bind('click', function() {
				snoozeAlarm(this);
			});

			alarmRow.children('.dismiss').bind('click', function() {
				dismissAlarm(this);
			});

			alarmRow.appendTo('.container');

			windowResize();

			var alarmIndex = globalAlarmIds.indexOf(alarmRow.data('alarm').id);
			if(alarmIndex!==-1)
				globalAlarmIds.splice(alarmIndex, 1);

			windowShow();
		}, diff);

		globalAlarmIds.push(alarmId);
		alarmRow.data('alarm', $.extend(alarm, {id: alarmId}));
	}
}

function snoozeAlarm(target) {
	var element = $(target);
	var row = element.parent();
	var alarm = row.data('alarm');

	var alarmMoment = moment().add(5, 'm');
	var previousOffset = alarmMoment.zone();
	setMomentTimezone(alarmMoment, alarm.realTimezone);
	var nextOffset = alarmMoment.zone();
	alarmMoment.add(previousOffset - nextOffset, 'm');

	setAlarm($.extend(alarm, {alarm: alarmMoment.toDate()}), 300000);

	row.remove();
	windowResize();

	windowHide();
}

function dismissAlarm(target) {
	var element = $(target);
	var row = element.parent();

	row.remove();
	windowResize();

	windowHide();
}

function setMomentTimezone(moment, timezone) {
	if(timezone===globalLocalTimezoneId) {
		moment.local();
	}
	else {
		moment.tz(timezone);
	}
}

function pointOnElement(point, element) {
	var xMin = element.position().left;
	var xMax = xMin + element.outerWidth();
	var yMin = element.position().top;
	var yMax = yMin + element.outerHeight();

	return point.X>=xMin && point.X<=xMax && point.Y>=yMin && point.Y<=yMax;
}

function windowShow() {
	if(!globalHasFocus) {
		globalHasFocus = true;
		windowOpen();
		globalAudioFile.load();
		globalAudioFile.play();
	}
}

function windowHide() {
	if(!$('.alarm-row').length) {
		globalAudioFile.pause();
		windowMinimize();
		globalHasFocus = false;
	}
}

function windowResize() {
	var containerWidth = 340;
	var windowHeight = $('html').height();
	var windowWidth = containerWidth;
	var scrollbarWidth = 0;

	if($('.alarm-row').length)
		scrollbarWidth = $('.container').width() - $('.alarm-row').outerWidth();
	$('.container').width(containerWidth + scrollbarWidth);

	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			overwolf.windows.changeSize(result.window.id, windowWidth + scrollbarWidth, windowHeight);
		}
	});
}

function windowOpen() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			overwolf.windows.restore(result.window.id);
		}
	});
}

function windowMinimize() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success') {
			overwolf.windows.minimize(result.window.id);
		}
	});
}

function initEyeX() {
	if(typeof overwolf==='undefined') {
		return true;
	}

	overwolf.extensions.current.getExtraObject('eyex', function(result) {
		if(result.status==='success') {
			var eyex = result.object;
			eyex.onGazePoint.addListener(onGazePointChanged);
			eyex.init();
		}
	});
}

function onGazePointChanged(gazePoint) {
	var smoothGazePoint = filterGazePoint(gazePoint);

	$('.snooze, .dismiss').each(function() {
		var element = $(this);

		if(smoothGazePoint.X<=$(window).width() && smoothGazePoint.Y<=$(window).height() && pointOnElement(smoothGazePoint, element)) {
			element.addClass('hovered');
			element.animate({opacity: 0}, 3000, function() {
				if(element.hasClass('snooze')) {
					snoozeAlarm(this);
				}
				else if(element.hasClass('dismiss')) {
					dismissAlarm(this);
				}
			});
		}
		else {
			element.stop(true).css('opacity', 1);
			element.removeClass('hovered');
		}
	});
}

function filterGazePoint(gazePoint) {
	// Throw away historic gaze point if its timestamp is too old
	var oldPointThresholdMilliseconds = 200;

	if(globalGazePoint===null || (gazePoint.Timestamp-globalGazePoint.Timestamp)>oldPointThresholdMilliseconds) {
		globalGazePoint = {X:gazePoint.X|0, Y:gazePoint.Y|0, Timestamp:gazePoint.Timestamp};
	}

	// Exponential smooting alpha value between 0 and 1:
	// - Low alpha value gives slow and stable movement
	// - High alpha value gives fast but nervous movement
	// For example: If alpha is 0.2, 20% of the new point is used in the weighted average
	var alpha = 0.3;

	// Distance threshold in pixels:
	// If the gaze point moves shorter than this distance, it can be due
	// to involuntary eye movements such as micro-saccades or just noise
	// in the system. In this case, the alpha value should be decreased.
	// Ideally, this threshold should be based on the eye-gaze vector angle
	// but in this implementation we chose pixels for simplicity reasons.
	var distanceTreshold = 60;

	distanceBetweenOldAndNewPoint = pointDistance(globalGazePoint, gazePoint);
	if(distanceBetweenOldAndNewPoint<distanceTreshold) {
		alpha = alpha/(distanceTreshold - distanceBetweenOldAndNewPoint);
	}

	// Exponential smoothing algorithm: resultPoint = newPoint * alpha + oldPoint * (1 - alpha)
	newGazePoint = {
		X:(gazePoint.X * alpha + globalGazePoint.X * (1 - alpha)),
		Y: (gazePoint.Y * alpha + globalGazePoint.Y * (1 - alpha)),
		Timestamp: gazePoint.Timestamp
	};

	// Save the filtered gaze point in memory to use it next time a new gaze point arrives.
	globalGazePoint = newGazePoint;

	return newGazePoint;
}

function pointDistance(point1, point2) {
	var xs = 0;
	var ys = 0;

	xs = point2.X - point1.X;
	xs = xs * xs;

	ys = point2.Y - point1.Y;
	ys = ys * ys;

	return Math.sqrt(xs + ys);
}
